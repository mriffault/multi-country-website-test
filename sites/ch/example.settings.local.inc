<?php

/**
 * @file
 * Drupal site-specific configuration file.
 */

/**
 * Database settings
 */
$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => 'bdd_ch',
      'username' => 'drupaluser',
      'password' => '',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

$conf['file_temporary_path'] = 'C:\TEMP';
$conf['devel_enable'] = TRUE;
$conf['cache'] = FALSE; //page cache
$conf['preprocess_css'] = FALSE; //optimize css
$conf['preprocess_js'] = FALSE; //optimize javascript

/**
 * Error reporting
 */
error_reporting(-1); // http://www.php.net/manual/fr/function.error-reporting.php
$conf['error_level'] = 2;
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

/**
* Base URL (optional).
*/
// $base_url = 'http://www.example.com'; // NO trailing slash!

/**
* Drupal automatically generates a unique session cookie name for each site
* based on its full domain name. If you have multiple domains pointing at the
* same Drupal site, you can either redirect them all to a single domain (see
* comment in .htaccess), or uncomment the line below and specify their shared
* base domain. Doing so assures that users remain logged in as they cross
* between your various domains.
*/
// $cookie_domain = '.havas-test.local';

?>
