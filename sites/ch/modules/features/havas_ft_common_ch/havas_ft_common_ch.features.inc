<?php
/**
 * @file
 * havas_ft_common_ch.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function havas_ft_common_ch_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
