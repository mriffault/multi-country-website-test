<?php

/**
 * @file
 * Example file for your local configuration file for Drupal's multi-site
 * directory aliasing feature.
 *
 * DO NOT PUT THIS FILE UNDER VERSION CONTROL
 */

$sites['www.havas-test.local.ca'] = 'ca';
$sites['www.havas-test.local.ch'] = 'ch';
$sites['www.havas-test.local.fr'] = 'fr';
$sites['www.havas-test.local.uk'] = 'uk';
