<?php

/**
 * @file
 * Configuration file for Drupal's multi-site directory aliasing feature.
 *
 * @see default.settings.php
 * @see conf_path()
 * @see http://drupal.org/documentation/install/multi-site
 */

$sites['www.havas-test.com'] = 'fr';
$sites['www.havas-test.com.ca'] = 'ca';
$sites['www.havas-test.com.ch'] = 'ch';
$sites['www.havas-test.com.uk'] = 'uk';

if (file_exists(DRUPAL_ROOT . '/sites/sites.local.inc')) {
  include_once DRUPAL_ROOT . '/sites/sites.local.inc';
}
