<?php

if (file_exists('./'. conf_path() .'/settings.local.inc')) {
  include_once './'. conf_path() .'/settings.local.inc';
}

if (file_exists(DRUPAL_ROOT . '/sites/settings.common.inc')) {
  include_once DRUPAL_ROOT . '/sites/settings.common.inc';
}

/**
 * Set up settings for France website
 *
 * - This file IS under version control (SVN or GIT), you share it with your
 *   co-worker, so EDIT IT CAREFULLY
 * - DO NOT write database credentials in this file
 */

/**
 * File system paths
 */
$conf['file_public_path'] = 'sites/ca/files';
$conf['file_private_path'] = 'sites/ca/files-private';

/**
 * Authorized file system operations:
 *
 * The Update manager module included with Drupal provides a mechanism for
 * site administrators to securely install missing updates for the site
 * directly through the web user interface. On securely-configured servers,
 * the Update manager will require the administrator to provide SSH or FTP
 * credentials before allowing the installation to proceed; this allows the
 * site to update the new files as the user who owns all the Drupal files,
 * instead of as the user the webserver is running as. On servers where the
 * webserver user is itself the owner of the Drupal files, the administrator
 * will not be prompted for SSH or FTP credentials (note that these server
 * setups are common on shared hosting, but are inherently insecure).
 *
 * Some sites might wish to disable the above functionality, and only update
 * the code directly via SSH or FTP themselves. This setting completely
 * disables all functionality related to these authorized file operations.
 *
 * @see http://drupal.org/node/244924
 *
 * Remove the leading hash signs to disable.
 */
# $conf['allow_authorize_operations'] = FALSE;
