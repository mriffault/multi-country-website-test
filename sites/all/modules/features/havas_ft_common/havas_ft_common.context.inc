<?php
/**
 * @file
 * havas_ft_common.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function havas_ft_common_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'common_front_blocks';
  $context->description = '';
  $context->tag = 'Common';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'bartik' => 'bartik',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'locale-language' => array(
          'module' => 'locale',
          'delta' => 'language',
          'region' => 'header',
          'weight' => '-9',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-9',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Common');
  $export['common_front_blocks'] = $context;

  return $export;
}
