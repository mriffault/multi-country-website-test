<?php
/**
 * @file
 * havas_ft_common_uk.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function havas_ft_common_uk_locale_default_languages() {
  $languages = array();

  // Exported language: en.
  $languages['en'] = array(
    'language' => 'en',
    'name' => 'English',
    'native' => 'English',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => '',
    'prefix' => 'en',
    'weight' => 0,
  );
  return $languages;
}
