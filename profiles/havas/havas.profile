<?php
/**
 * @file
 * Enables modules and site configuration for a Havas site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function havas_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks_alter()
 */
function havas_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_finished']['function'] = 'havas_install_finished';
  $tasks['install_finished']['display_name'] = t('Finished');
  $tasks['install_finished']['type'] = 'normal';
}

/**
 * We override the last task of the Drupal installation, to perform some actions
 * after all modules have been enabled.
 * source: http://drupal.stackexchange.com/a/144208/14759
 */
function havas_install_finished(&$install_state) {
  drupal_set_title(st('@drupal installation complete', array('@drupal' => drupal_install_profile_distribution_name())), PASS_THROUGH);
  $messages = drupal_set_message();
  $output = '<p>' . st('Congratulations, you installed @drupal!', array('@drupal' => drupal_install_profile_distribution_name())) . '</p>';
  $output .= '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';

  // remove all blocks from all Regions (because our layout is Context-based)
  db_query("UPDATE block set status=0, region=-1 WHERE status=1;");

  return $output;
}
